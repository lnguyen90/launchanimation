package com.longnguyen.launchanimation;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onResume() {
        super.onResume();
        animate();
    }

    private void animate() {
        boolean shouldFinish = false;
        if(getIntent().getExtras() != null) {
            shouldFinish = getIntent().getExtras().getBoolean("should_finish");
        }

        if(shouldFinish) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent mainActivity = new Intent(Intent.ACTION_MAIN);
                    mainActivity.addCategory(Intent.CATEGORY_HOME);
                    mainActivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(mainActivity);
                    finish();
                }
            }, 500);
        } else {
            goToActivity2();
        }
    }

    private void goToActivity2() {
        Intent intent = new Intent(this, Main2Activity.class);
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.slide_up, 0);
    }
}
